# Blog application #

This application is built with Ruby on Rails. It has been created as an ongoing project for a course called "Web Application Architectures" provided by www.coursera.com.

### To run the project, run the following terminal commands: ###

* git clone git@bitbucket.org:joselee/blog.git
* cd blog
* rake db:migrate
* rails server

### Project dependencies: ###

* RVM 1.25.18 (or higher)
* Ruby 2.1.2 (or higher)
* Rails 4.1.4 (or higher)